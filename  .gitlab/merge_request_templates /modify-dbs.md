# Modify databases

## Summary table

_Modify with your request_

| Action | Entity name  | short name | Environment |
| ------ | ------------ | ---------- | ----------- |
| add    | Example coop | example    | trial       |
| del    | Coco coop    | coco       | trial       |
| add    | Coco coop    | coco       | prod        |
| ...    | ...          | ...        | ...         |

## Human description

_Modify with your request_ Hey! The "example" people want to do the trial and Coco coop is ready to go to prod.

---

## Checklists

### Changes at this repo

For each database change, please do:

- [ ] At host vars file, add or delete the **short name** to/from `odoo_role_odoo_dbs`
- [ ] At host vars file, add or delete the **domain name** to/from `domains`

### Changes at 3d party servers

- [ ] At the DNS provider, add or delete a CNAME record from the **domain name** to the **root domain**

### Glossary

_Don't modify this one, it's only informative_

| environment | `host_vars` file to modify | domain name | root domain |
| ----------- | -------------------------- | ---------- | ---------- |
| trial       | `inventory/host_vars/proves.processos.org/config.yml`| `example.proves.processos.org` | `proves.processos.org` |
| prod        | `inventory/host_vars/processos.org/config.yml`       | `example.processos.org`        |`processos.org`         |

---

## After this MR is approved

1. Run `odoo-provisioning` with this inventory against trial and/or prod servers.
2. Follow [these steps](https://github.com/coopdevs/handbook/wiki/Passos-configurar-nova-empresa-a-Odoo) in order to create the company/ies.